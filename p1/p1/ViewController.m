//
//  ViewController.m
//  p1
//
//  Created by Click Labs134 on 9/4/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
UILabel * alabel;
UILabel * alabel1;
    UITextField *atextfield;
    UITextField *atextfield1;
    UITextField *atextfield2;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel * alabel2 = [[UILabel alloc] initWithFrame: CGRectMake(50, 50, 290, 60)];
    alabel2.text = @"                       FACEBOOK";
    alabel2.textColor = [UIColor whiteColor];
    alabel2.backgroundColor = [UIColor blueColor];
    [self.view addSubview: alabel2];

    alabel = [[UILabel alloc] initWithFrame: CGRectMake(50, 200, 230, 40)];
    alabel.text = @"Email id";
    alabel.textColor = [UIColor blueColor];
    alabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview: alabel];
    
    alabel1 = [[UILabel alloc] initWithFrame: CGRectMake(50, 300, 230, 40)];
    alabel1.text = @"Password";
    alabel1.textColor = [UIColor blueColor];
    alabel1.backgroundColor = [UIColor clearColor];
    [self.view addSubview: alabel1];
    

    UIButton * abutton = [[UIButton alloc] initWithFrame: CGRectMake(50, 500, 100, 40)];
    [abutton setTitle: @"LOGIN" forState: UIControlStateNormal];
    abutton.backgroundColor = [UIColor blueColor];
    [abutton addTarget: self action : @selector(LOGIN:) forControlEvents: UIControlEventTouchUpInside ];
    [self.view addSubview: abutton];
    
    UIButton * abutton1 = [[UIButton alloc] initWithFrame: CGRectMake(200, 500, 100, 40)];
    [abutton1 setTitle: @"SIGNUP" forState: UIControlStateNormal];
    abutton1.backgroundColor = [UIColor blueColor];
    [abutton1 addTarget: self action : @selector(SIGNUP :) forControlEvents: UIControlEventTouchUpInside ];
    [self.view addSubview: abutton1];

    atextfield = [[UITextField alloc]initWithFrame: CGRectMake(50, 250, 230, 40)];
    atextfield.placeholder = @"enter your valid email id";
    atextfield.textColor = [UIColor blackColor];
    atextfield.backgroundColor = [UIColor clearColor];
    atextfield.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview: atextfield];
    
    atextfield1 = [[UITextField alloc]initWithFrame: CGRectMake(50, 350, 230, 40)];
    atextfield1.placeholder = @"enter your password";
    atextfield1.textColor = [UIColor blackColor];
    atextfield1.backgroundColor = [UIColor clearColor];
    atextfield1.borderStyle = UITextBorderStyleRoundedRect;
   [atextfield1 setSecureTextEntry:YES];
    [self.view addSubview: atextfield1];
    
    atextfield2 = [[UITextField alloc]initWithFrame: CGRectMake(50, 380, 230, 40)];
    atextfield2.placeholder = @"type your password again";
    atextfield2.textColor = [UIColor blackColor];
    atextfield2.backgroundColor = [UIColor clearColor];
    atextfield2.borderStyle = UITextBorderStyleRoundedRect;
    [atextfield2 setSecureTextEntry:YES];
    [self.view addSubview: atextfield2];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"fb2.jpeg"]]];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)LOGIN: (UIButton *) sender{

    if ([atextfield1.text isEqualToString: atextfield2.text]) {
        [sender setTitle:@"LOGIN SUCCESSFUL" forState:(UIControlStateNormal)];
    } else {
     [sender setTitle:@"LOGIN UNSUCCESSFUL" forState:(UIControlStateNormal)];
    }
    
    sender.titleLabel.font = [UIFont systemFontOfSize:8];
    
   
}
-(void)SIGNUP: (UIButton *) sender{
    //NSLog(@"ALREADY A USER");
    
    /*if ([validateEmailWithString.text isEqualToString: atextfield.text]) {
        [sender setTitle:@"SIGN UP FAILED" forState:(UIControlStateNormal)];
    }
    else {
        [sender setTitle:@"SIGNED UP" forState:(UIControlStateNormal)];
    }*/
    sender.titleLabel.font = [UIFont systemFontOfSize:8];
}


/*- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
